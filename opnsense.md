# OPNSense

**Sito web: **[https://opnsense.org/](https://opnsense.org/ "Sito Ufficiale OPNSense")

**Funzionalità principali:**

Firewall perimetrale di tipo stateful  con funzioni aggiuntive di router, Access Point wireless, captive portal, server DHCP, traffic shaper, High Availability CARP, Network Monitoring, server DNS e VPN end-to-end; utilizzabile tramite interfaccia web, supporta i backup su Google Drive.

**Vantaggi per le aziende:**

Creazione di un'infrastruttura multifunzionale destinata alla sicurezza del comparto IT senza l'acquisto di costoso hardware dedicato.

**Esempio pratico di utilizzo:**

Dispositivo per la rete con funzioni avanzate per la rete, fornisce strumenti  per la sicurezza su tutti i fronti.

**Licenza:** FreeBSD v.2

