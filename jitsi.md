# Jitsi

**Sito web: **[https://jitsi.org/](/Sito Ufficiale Jitsi)

**Funzionalità principali:**

Software di webconference con  chat e condivisione del desktop.

**Vantaggi per le aziende:**

Software web, non richiede alcuna installazione all'utente finale.

**Esempio pratico di utilizzo:**

Riunione mensile aziendale tra dipendenti di filiali localizzate in città diverse.

**Licenza:** Apache License v.2

