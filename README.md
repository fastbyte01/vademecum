# ![](/assets/logoiisb.png)

# Il software libero in Azienda

Cari colleghi e colleghe, quando parliamo di software libero ci viene spesso chiesto di evidenziare, con esempi concreti, cosa è possibile realizzare in azienda.

Con questo vademecum presentiamo alcuni dei migliori software liberi per le imprese.

L'esposizione è fatta senza collegamenti funzionali o tecnologici tra i software, in modo che le schede risultino degli spunti iniziali per ulteriori approfondimenti conoscitivi, sia da parte dell'utente finale che del professionista.

Per ulteriori approfondimenti sui software citati nelle schede, vi invitiamo a consultare il sito [www.industriasoftwarelibero.it](/www.industriasoftwarelibero.it "Sito web Industri Italiana Software Libero") ed a prendere contatto con l'associato di Industria più vicino a voi.

