# Darik's Boot and Nuke

**Sito web: **[http://www.dban.org](http://www.dban.org "Sito ufficiale Dban")

**Funzionalità principali:**

Darik’s Boot and Nuke \(DBAN\) è una delle soluzioni più semplici per distruggere le informazioni personali e aziendali presenti su un hard disk.

**Vantaggi per le aziende:**

Permette di eliminare in modo sicuro i dati presenti sugli hard disk prima che questi vengano dismessi o ceduti.

**Esempio pratico di utilizzo:**

Eliminazione sicura di dati aziendali e personali  prima della dismissione o della cessione dei computer aziendali.

**Licenza: **GPL v.2

