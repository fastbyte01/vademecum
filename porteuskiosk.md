# PorteusKiosk

**Sito web:**[ http://porteus-kiosk.org/](http://porteus-kiosk.org/ "Sito Ufficiale Porteus Kiosk")

**Funzionalità principali:**

Sistema Operativo ridotto e confinato al solo utilizzo dell'applicativo che in questo caso ne compone il desktop, ovvero i browser web Mozilla Firefox o Google Chrome; permette di impostare delle regole per mantenere la privacy e confinare l'utilizzo della macchina alla sola navigazione web.

**Vantaggi per le aziende:**

Sicurezza e produttività specialmente nell'utilizzo di applicazioni in cloud, riutilizzo di hardware obsoleto.

**Esempio pratico di utilizzo:**

Thin client semplice e funzionale, da impiegare in caso si voglia una postazione di lavoro sicura.

**Licenza:** GPL v.2

