# Pentaho

**Sito web: **[http://www.pentaho.com/](http://www.pentaho.com/ "Sito ufficiale pentaho")

**Funzionalità principali:**

Pentaho fornisce soluzioni di completa integrazione dei dati e di Business Analytics. Permette di gestire e integrare dati provenienti da qualsiasi fonte e trasformarli in informazione per ottimizzare il decision making attraverso servizi di report e dashboard interattivi e completamente personalizzabili.

**Vantaggi per le aziende:**

Permette di tenere sotto controllo il proprio business offrendo tutte le funzionalità di integrazione dei dati, ETL, analisi interattive e data minning; le analisi degli elaborati, se opportunamente strutturate, riescono ad essere molto articolare e flessibili.

**Esempio pratico di utilizzo:**

Analisi dei dati di vendita delle filiali aziendali, divise per area geografica, tipologia di clienti e prodotti venduti.

**Licenza:** GPL v.2

