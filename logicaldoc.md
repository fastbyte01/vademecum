# LogicalDOC

**Sito web: **[https://www.logicaldoc.com/](https://www.logicaldoc.com/ "Sito web ufficiale LogicalDOC")

**Funzionalità principali:**

Sistema di gestione documentale con cui è possibile ottenere il controllo sui propri documenti, con particolare attenzione al recupero veloce dei contenuti e all'automazione dei processi aziendali.

**Vantaggi per le aziende:**

Software in cloud accessibile via web, i documenti dell'azienda rimangono disponibili da qualunque postazione internet.

**Esempio pratico di utilizzo:**

Archiviazione ed indicizzazione della documentazione contabile d'impresa.

**Licenza:** GNU LGPL

