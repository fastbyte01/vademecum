# Roundcube

**Sito web: **[https://roundcube.net/](https://roundcube.net/ "Sito web ufficiale roundcube")

**Funzionalità principali:**

Web-mail per la gestione di caselle email online.

**Vantaggi per le aziende:**

Possibilità di accedere alla mail da qualsiasi postazione connessa ad internet.

**Esempio pratico di utilizzo:**

** L**ettura della posta aziendale dallo smartphone.

**Licenza:** GPL v. 3

