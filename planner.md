# Planner

**Sito web: **[https://wiki.gnome.org/Apps/Planner](https://wiki.gnome.org/Apps/Planner "Sito ufficiale Planner")

**Funzionalità principali:**

Un'applicazione per la gestione di progetti tramite creazione di diagrammi Gantt, creazione di gruppi di risorse umane da assegnare ai task di progetto e creazione della Work Breakdown Structure.

**Vantaggi per le aziende:**

Facilità di installazione, interfaccia semplice e intuitiva, non richiede grandi risorse per l’esecuzione.

**Esempio pratico di utilizzo:**

Creare e calendarizzare rapidamente un progetto inserendo i task che lo compongono e per ognuno assegnare risorse umane.

**Licenza:** GPL v.2

