# ODOO OCA

**Sito web: **[https://odoo-community.org/](https://odoo-community.org/ "Sito ufficiale Odoo OCA")

**Funzionalità principali:**

Framework per la sviluppo veloce di applicazioni aziendali in cloud computing.

**Vantaggi per le aziende:**

Rapidità di sviluppo di applicazioni per la gestione aziendale integrata, possibilità di personalizzazione dell'ERP sui processi aziendali.

**Esempio pratico di utilizzo:**

Sviluppo personalizzato di applicazioni per la gestione di progetti.

**Licenza:** AGPL v.3

