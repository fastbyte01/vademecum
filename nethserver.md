# Nethserver

**Sito web: **[http://www.nethserver.org/](http://www.nethserver.org/ "sito ufficiale Nethserver")

**Funzionalità principali:**

Sistema operativo basato su Linux - CentOS dal design modulare, con funzioni di web server, mail server, web-filter, VPN server e IPS/IDS; può essere facilmente gestito tramite interfaccia web.

**Vantaggi per le aziende:**

Rapido deploy di server e appliance di sicurezza, grande suite di strumenti a disposizione e ottimo livello di sicurezza e stabilità operativa.

**Esempio pratico di utilizzo:**

** **Creazione di un server polifunzionale e personalizzabile a seconda delle proprie esigenze.

**Licenza:** GPL v.3

